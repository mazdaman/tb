<?php
session_start();
//Load all functions
require_once('load.php');

login_check();
?>

    <!DOCTYPE html>
    <html>

    <head>
        <title>
            <?php echo get_site_name();?>
        </title>

        <?php echo $Header->head();?>
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
    </head>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">

                <?php echo $Header->header();?>

                <!-- page content -->
                <div class="right_col" role="main">



                    <div class="col-md-4 col-sm-10 col-xs-12">
                        <div class="x_panel tile fixed_height_500">
                            <div class="x_title">
                                <h2>Weather</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">

                                <?php

                                    $query = @unserialize (file_get_contents('http://ip-api.com/php/'));
                                    if ($query && $query['status'] == 'success') {
                                        $country = $query['country'];
                                        $city = $query['city'];
                                        $long = $query['lon'];
                                        $lat = $query['lat'];
                                        $api_key = "33cd7ef348dda203038b2739d9f1a082";
                                        $weather = file_get_contents('http://api.openweathermap.org/data/2.5/weather?lat=' . $lat . '&lon=' . $long . '&appid=' .$api_key);
                                    $json = json_decode($weather);
                                    $location_key = $json->id;
                                    }else{
                                     echo "Sorry but we are unable to gather you location at the moment.";   
                                    }

                                    
                                    
                                    ?>

                                    <div id="openweathermap-widget-15"></div>
                                    <script>
                                        window.myWidgetParam ? window.myWidgetParam : window.myWidgetParam = [];
                                        window.myWidgetParam.push({
                                            id: 15,
                                            cityid: <?php echo $location_key;?>,
                                            appid: '33cd7ef348dda203038b2739d9f1a082',
                                            units: 'metric',
                                            containerid: 'openweathermap-widget-15',
                                        });
                                        (function() {
                                            var script = document.createElement('script');
                                            script.async = true;
                                            script.charset = "utf-8";
                                            script.src = "//openweathermap.org/themes/openweathermap/assets/vendor/owm/js/weather-widget-generator.js";
                                            var s = document.getElementsByTagName('script')[0];
                                            s.parentNode.insertBefore(script, s);
                                        })();

                                    </script>
                            </div>
                        </div>


                        <div class="x_panel tile fixed_height_500">
                            <div class="x_title">
                                <h2>Photos</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <div class="dashboard-widget-content">
                                    <ul class="quick-list">
                                        <li> <i class="fa fa-sign-cogs"></i> <a href="<?php echo site_url();?>/pictures/">Manage Pictures</a> </li>
                                    </ul>
                                </div>
                                <table border="2" style="width:100%">
  <tr>
    <th></th>
    <th></th> 
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>

</table>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-10 col-xs-12">
                        <div class="x_panel tile fixed_height_500">
                            <div class="x_title">
                                <h2>News</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <?php
                                $xml = 'http://feeds.bbci.co.uk/news/rss.xml' ;
                                $xmlDoc = new DOMDocument();
                                $xmlDoc->load($xml);
                                $items = $xmlDoc->getElementsByTagName('item');
                                foreach($items as $key => $item) {
                                    $item_title= $item->getElementsByTagName('title')->item(0)->childNodes->item(0)->nodeValue;
                                    $item_link= $item->getElementsByTagName('link')->item(0)->childNodes->item(0)->nodeValue;
                                    $item_desc= $item->getElementsByTagName('description')->item(0)->childNodes->item(0)->nodeValue;
                                    $media = $item->getElementsByTagNameNS('http://search.yahoo.com/mrss/', 'thumbnail');
                                    foreach($media as $thumb) {
                                        $thumb_url = $thumb->getAttribute('url');
                                        $thumbnail = base64_encode(file_get_contents($thumb_url));
                                    }
                                ?>
                                    <img height="200px" width="300px" src="data:image/x-icon;base64,<?php echo $thumbnail; ?>">
                                    <p>
                                        <strong><a target="_blank" href="<?php echo $item_link; ?>"><?php echo $item_title; ?></a></strong>
                                    </p>
                                    <p>
                                        <?php echo $item_desc;?>
                                    </p>
                                    <?php
                                    break;
                                }
                                ?>
                            </div>
                        </div>


                        <div class="x_panel tile fixed_height_500">
                            <div class="x_title">
                                <h2>Tasks</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <div class="dashboard-widget-content">
                                    <ul class="quick-list">
                                        <li> <i class="fa fa-plus"></i> <a href="<?php echo site_url();?>/add-new-task/">Add new task</a> </li>
                                        <li> <i class="fa fa-cogs"></i> <a href="<?php echo site_url();?>/tasks/" >Manage tasks</a> </li>
                                    </ul>                                    
                                    
                                    <?php
                                    echo $Widgets->dashboard__tasks__view();
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-10 col-xs-12">
                        <div class="x_panel tile fixed_height_500">
                            <div class="x_title">
                                <h2>Sports</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">

                                <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive datatable-buttons" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Home</th>
                                            <th></th>
                                            <th>Away</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                        $winner_label = '<label class="label label-success">won</label>';
                        $loser_label = '<label class="label label-danger">lost</label>';
                        $draw_label = '<label class="label label-warning">draw</label>';
                        $fileHandle = fopen("files/I1.csv", "r");
                        $row_count = 1;
                        while (($row = fgetcsv($fileHandle, 0, ",")) !== FALSE) {
                            if($row_count!=1){
    
						?>
                                            <tr>
                                                <td>
                                                    <?php echo $row[2];?>
                                                    <div align="right">
                                                        <?php 
                                if($row[4]>$row[5]){echo $winner_label;}elseif($row[4]==$row[5]){echo $draw_label;}else{echo $loser_label;}?>
                                                        <span class="badge"><?php echo $row[4];?></span></div>
                                                </td>
                                                <td class="text-center">
                                                    vs.
                                                </td>
                                                <td>
                                                    <?php echo $row[3];?>
                                                    <div align="right">
                                                        <?php  
                                if($row[5]>$row[4]){echo $winner_label;}elseif($row[4]==$row[5]){echo $draw_label;}else{echo $loser_label;}?>
                                                        <span class="badge"><?php echo $row[5];?></span></div>
                                                </td>

                                            </tr>
                                            <?php
                        }
                            $row_count++;
                        }
                        
                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>


                        <div class="x_panel tile fixed_height_500">
                            <div class="x_title">
                                <h2>Clothes</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <?php
                                $json_string = 'files/clothing-api.json';
                                $jsondata = file_get_contents($json_string);
                                $obj = json_decode($jsondata,true);

                                $count_clothes = 1;
                                foreach($obj as $i):
                                if($count_clothes!=1){
                                    
                                $jumper = 0;
                                $hoodie = 0;
                                $jacket = 0;
                                $sweater = 0;
                                $raincoat = 0;
                                $blazer = 0;
                                    
                                    for($x = 0; $x <= count($i)-1; $x++) {
                                        $item = $i[$x]['clothe'];
                                        if($item == "jumper"){
                                            $jumper++;
                                        }
                                        
                                        elseif($item == "hoodie"){
                                            $hoodie++;
                                        }
                                        elseif($item == "jacket"){
                                            $jacket++;
                                        }
                                        elseif($item == "sweater"){
                                            $sweater++;
                                        }
                                        elseif($item == "blazer"){
                                            $blazer++;
                                        }
                                        elseif($item == "raincoat"){
                                            $raincoat++;
                                        }else{
                                            echo $item;
                                            echo "<br>";
                                        }
                                    } 
                                }
                                $count_clothes++;
                                endforeach;

                                
                                ?>

<div id="container3" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                                    <script>
                                        Highcharts.chart('container3', {
                                            chart: {
                                                plotBackgroundColor: null,
                                                plotBorderWidth: null,
                                                plotShadow: false,
                                                type: 'pie'
                                            },
                                            title: {
                                                text: 'Clothes Distribution over 1000 days'
                                            },
                                            tooltip: {

                                            },
                                            plotOptions: {
                                                pie: {
                                                    allowPointSelect: true,
                                                    cursor: 'pointer',
                                                    dataLabels: {
                                                        enabled: true,
                                                        style: {
                                                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                        }
                                                    }
                                                }
                                            },
                                            series: [{
                                                name: 'Brands',
                                                colorByPoint: true,
                                                data: [{
                                                    name: 'Jacket',
                                                    y: <?php echo $jacket;?>
                                                }, {
                                                    name: 'Jumper',
                                                    y: <?php echo $jumper?>
                                                }, {
                                                    name: 'Hoodie',
                                                    y: <?php echo $hoodie;?>
                                                }, {
                                                    name: 'Sweater',
                                                    y: <?php echo $sweater;?>
                                                }, {
                                                    name: 'Raincoat',
                                                    y: <?php echo $raincoat;?>
                                                }, {
                                                    name: 'Blazer',
                                                    y: <?php echo $blazer;?>
                                                }]
                                            }]
                                        });

                                    </script>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /page content -->

                <!-- footer content -->
                <?php echo $Footer->footer();?>
                <!-- /footer content -->
            </div>
        </div>

    </body>

    </html>
