<?php 
// if accessed directly than exit
if (!defined('ABSPATH')) exit;

if( !class_exists('Widgets') ):
	class Widgets{
		private $database;
		private $current__user__id;
		private $current__user;
		function __construct() {
			global $db;
			$this->database = $db;
			$this->current__user__id = get_current_user_id();
			$this->current__user = get_userdata($this->current__user__id);
		}


		public function dashboard__tasks__view(){
			ob_start();
            
            

            
            $notfication__query = " ORDER BY `priority` DESC LIMIT 0,3";
            $args= array('user_ID' => $this->current__user__id);
			$results = get_tabledata(TBL_TASKS,false,$args,$notfication__query);

			if(!$results):
				echo page_not_found("There are currently no tasks added",'  ',false);
			else:
			?>
<table id="datatable-buttons" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>
                <?php _e('Task');?>
            </th>
            <th>
                <?php _e('Priority');?>
            </th>
            <th class="text-center">Status</th>
        </tr>
    </thead>
    <tbody>
        <?php if($results): foreach($results as $row): ?>
        <tr>

            <td <?php if($row->status==1){?> bgcolor="#a7ff7f"
                <?php }else{?>bgcolor="#f7a271"
                <?php };?>>
                <?php _e($row->task);?>
            </td>
            <td>
                <?php _e($row->priority);?>
            </td>
            <td class="text-center">
                <label>
									<input type="checkbox" class="js-switch" <?php checked($row->status , 1);?> onClick="javascript:approve_switch(this);" data-id="<?php echo $row->ID;?>" data-action="task_status_change"/>
								</label>
            </td>
        </tr>
        <?php endforeach; endif; ?>
    </tbody>
</table>
<?php endif; ?>
<?php
			$content = ob_get_clean();
			return $content;
		}

		public function all__tasks__page(){
			ob_start();
            $args= array('user_ID' => $this->current__user__id);
			$results = get_tabledata(TBL_TASKS,false,$args);

			if(!$results):
				echo page_not_found("Oops! There is no tasks found",'  ',false);
			else:
			?>
    <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap datatable-buttons" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>
                    <?php _e('Task');?>
                </th>
                <th>
                    <?php _e('Priority');?>
                </th>
                <th class="text-center">Status</th>
                <th class="text-center">Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php if($results): foreach($results as $row): ?>
            <tr>
                <td <?php if($row->status==1){?> bgcolor="#a7ff7f"
                    <?php }else{?>bgcolor="#f7a271"
                    <?php };?>>
                    <?php _e($row->task);?>
                </td>
                <td>
                    <?php _e($row->priority);?>
                </td>
                <td class="text-center">
                    <label>
									<input type="checkbox" class="js-switch" <?php checked($row->status , 1);?> onClick="javascript:approve_switch(this);" data-id="<?php echo $row->ID;?>" data-action="task_status_change"/>
								</label>
                </td>
                <td class="text-center">
                    <a href="<?php echo site_url();?>/edit-task/?user_id=<?php echo $row->ID;?>" class="btn btn-dark btn-xs"><i class="fa fa-edit"></i> Edit</a>
                    <a href="#" class="btn btn-danger btn-xs" onclick="javascript:delete_function(this);" data-id="<?php echo $row->ID;?>" data-action="delete_task"><i class="fa fa-trash"></i> Delete</a>
                </td>
            </tr>
            <?php endforeach; endif; ?>
        </tbody>
    </table>
    <?php endif; ?>
    <?php
			$content = ob_get_clean();
			return $content;
		}
        
        
        
        
        		public function all__pictures__page(){
			ob_start();
                    
            $args= array('user_ID' => $this->current__user__id);
			$results = get_tabledata(TBL_PICTURES,false,$args);

			if(!$results):
				echo page_not_found("There are currently no pictures added",'  ',false);
			else:
			?>
        <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap datatable-buttons" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>
                        <?php _e('Picture');?>
                    </th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php if($results): foreach($results as $row): ?>
                <tr>
                    <td> <img src="<?php echo "../".$row->picture;?>" alt="<?php echo "../".$row->picture;?>" height="200" width="200"></td>
                    <td class="text-center">
                        <a href="#" class="btn btn-danger btn-xs" onclick="javascript:delete_function(this);" data-id="<?php echo $row->ID;?>" data-action="delete_picture"><i class="fa fa-trash"></i> Delete</a>
                    </td>
                </tr>
                <?php endforeach; endif; ?>
            </tbody>
        </table>
        <?php endif; ?>
        <?php
			$content = ob_get_clean();
			return $content;
		}
        
        		public function task__status__change__process(){
			extract($_POST);
			$return = array(
				'status' => 0,
				'message_heading' => 'Failed !',
				'message' => 'Could not update task status, Please try again ',
				'reset_form' => 0
			);
				$user = get_userdata($id);	
				$args = array('ID' => $id);
				$result = $this->database->update(TBL_TASKS,array('status' => $status),$args);
			
				if($result):
					if($status == 0){
						$notification_args = array(
							'title' => ucfirst($user->user_role).' Task not completed',
							'notification' => 'You have successfully marked as incomplete a task.',
						);
						$return['message'] = 'You have successfully marked as incomplete a task.';
					}else{
						$notification_args = array(
							'title' => ucfirst($user->user_role).' Task completed',
							'notification' => 'You have successfully Completed a task',
						);
						$return['message'] = 'You have successfully marked a task as complete.';
					}
					add_user_notification($notification_args);
					$return['status'] = 1;
					$return['message_heading'] = 'Success !';
				endif;
			return json_encode($return);
		}
        
        
        		public function delete__task__process(){
			extract($_POST);
				$args  = array('ID' => $id);
				$result = $this->database->delete(TBL_TASKS,$args);
				if($result):
					$notification_args = array(
						'title' => 'Task deleted',
						'notification' => 'You have successfully deleted task ('.$id.').',
					);
					add_user_notification($notification_args);
					return 1;
				else:
					return 0;
				endif;
		}

                		public function delete__picture__process(){
			extract($_POST);
                            $args= array('ID' => $id);
                            $fn = null;
                            $results = get_tabledata(TBL_PICTURES,false,$args);
                            $fn = "..".$results[0]->picture;
                            chown($fn,465);
                            unlink(ABSPATH . CONTENT . $upload_str);

                            				$result = $this->database->delete(TBL_PICTURES,$args);
				if($result):
					$notification_args = array(
						'title' => 'picture deleted',
						'notification' => 'You have successfully deleted picture ('.$fn.').',
					);
					add_user_notification($notification_args);
					return 1;
				else:
					return 0;
				endif;
		}


        		public function add__task__page(){
			ob_start();
			if(!user_can('add_user')):
				echo page_not_found('Oops ! You are not allowed to view this page.','Please check other pages !');
			else:
			?>
            <form class="add-task submit-form" method="post" autocomplete="off">

            <div class="form-group col-sm-4 col-xs-12">
				<label for="date-of-fault">Task</label>
			<input type="text" name="task" class="form-control " /> </div>
				<div class="form-group col-sm-4 col-xs-12">
				<label for="">Priority</label>
				<br/>
				<label>
			<input type="radio" class="flat" value="1" name="priority"/> Low</label>
				<label>&nbsp;</label>
				<label>
			<input type="radio" class="flat" value="2" name="priority"/> Medium</label>
                <label>
				<input type="radio" class="flat" value="3" name="priority" value="3"/> High</label>
			</div>


                <div class="ln_solid"></div>

                <div class="form-group">
                    <input type="hidden" name="action" value="add_new_task" />
                    <button class="btn btn-success btn-md" type="submit">Add New Task</button>
                </div>
            </form>
            <?php echo $this->upload__image__section();?>
            <?php
			endif;
			$content = ob_get_clean();
			return $content;
		}        
        
        		public function add__picture__page(){
			ob_start();
			if(!user_can('add_user')):
				echo page_not_found('Oops ! You are not allowed to view this page.','Please check other pages !');
			else:
			?>
            <form class="add-picture submit-form" method="post" autocomplete="off">




                <div class="row">
                    <div class="col-xs-12 col-sm-6 form-goup">
                        <label>Upload Profile Image</label>
                        <input type="text" name="profile_img" class="form-control" value="" placeholder="Uploaded image url" readonly="readonly" />
                        <br/>
                        <a href="#" class="btn btn-default btn-sm" data-toggle="modal" data-target="#upload-image-modal"><i class="fa fa-camera"></i>&nbsp;Upload Image</a>
                    </div>
                    <div class="col-xs-12 col-sm-6 form-group">
                        <div class="profile-image-preview-box">
                            <img src="" class="img-responsive img-thumbnail" />
                        </div>
                    </div>
                </div>


                <div class="ln_solid"></div>

                <div class="form-group">
                    <input type="hidden" name="action" value="add_new_picture" />
                    <button class="btn btn-success btn-md" type="submit">Add New Picture</button>
                </div>
            </form>
            <?php echo $this->upload__image__section();?>
            <?php
			endif;
			$content = ob_get_clean();
			return $content;
		}
        
        
        		public function upload__image__section(){
			ob_start();
			?>
                <div class="modal fade" id="upload-image-modal" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title green" id="myModalLabel">Upload Image</h4>
                            </div>
                            <form class="upload-profile-image" method="post" autocomplete="off">
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="question">Choose an image for upload<span class="required">*</span></label>
                                        <input type="file" name="photo" accept="image/*" />
                                        <span class="help-block green">Supported image formats: jpeg, png, jpg</span>
                                        <span class="help-block green">Recommended profile image size: 250 x 250 pixels</span>
                                    </div>
                                    <div class="form-group">
                                        <div class="alert alert-success">
                                            <i class="fa fa-circle-o-notch fa-spin" aria-hidden="true"></i> Image is being upload, please do not close upload box ..
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <input type="hidden" name="action" value="upload_image" />
                                    <!--<button class="btn btn-success" type="submit">Submit</button>-->
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <?php
			$content = ob_get_clean();
			return $content;
		}
        
        
		public function add__picture__process(){
			extract($_POST);
			$return = array(
				'status' => 0,
				'message_heading' => 'Failed !',
				'message' => 'Could not add picture, Please try again.',
				'reset_form' => 0
			);
			

					$guid = get_guid(TBL_PICTURES);
					$result = $this->database->insert(TBL_PICTURES,
						array(
							'ID' => $guid,
                            'picture' => $profile_img,
							'user_ID' => $this->current__user__id,
						)
					);
						$user__id = $guid;
						$notification_args = array(
							'title' => 'New picture added',
							'notification' => 'You have successfully added a new picture.',
						);
						add_user_notification($notification_args);
						$return['status'] = 1;
						$return['message_heading'] = 'Success !';
						$return['message'] = 'Picture has been successfully added.';
						$return['reset_form'] = 1;
			
			return json_encode($return);
		}
        
        
		public function add__task__process(){
			extract($_POST);
			$return = array(
				'status' => 0,
				'message_heading' => 'Failed !',
				'message' => 'Could not add task, Please try again.',
				'reset_form' => 0
			);
			

					$guid = get_guid(TBL_TASKS);
					$result = $this->database->insert(TBL_TASKS,
						array(
							'ID' => $guid,
                            'task' => $task,
							'priority' => $priority,
							'user_ID' => $this->current__user__id,
						)
					);
						$user__id = $guid;
						$notification_args = array(
							'title' => 'New task added',
							'notification' => 'You have successfully added a new task.',
						);
						add_user_notification($notification_args);
						$return['status'] = 1;
						$return['message_heading'] = 'Success !';
						$return['message'] = 'task has been successfully added.';
						$return['reset_form'] = 1;
			
			return json_encode($return);
		}
        
        		public function upload__image__process(){
			$upload_str = '/uploads/user_images/'.$this->current__user__id.'/';
			$upload_img = ABSPATH . CONTENT . $upload_str;
			$upload_url = '/content'.$upload_str;
			if (!file_exists($upload_img))
				mkdir($upload_img, 0755, true);
			if(isset($_FILES["photo"]) && $_FILES["photo"]["size"] > 0){
				$sourcePath = $_FILES["photo"]["tmp_name"];
				$file = pathinfo($_FILES['photo']['name']);
				$fileType = $file["extension"];
				$full_img = 'img-'.time().'.'.$fileType;
				createThumb($sourcePath, $upload_img.$full_img,$fileType,250,250,250);
				return $upload_url.$full_img;
			}else{
				return 0;
			}
		}
        
	}
endif;
?>
