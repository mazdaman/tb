<?php 
// if accessed directly than exit
if (!defined('ABSPATH')) exit;

if( !class_exists('User') ):
	require_once( ABSPATH . CONTENT . '/class/class.user.php');
endif;

$User = new User();

if( !class_exists('Profile') ):
	require_once( ABSPATH . CONTENT . '/class/class.profile.php');
endif;

$Profile = new Profile();

if( !class_exists('Settings') ):
	require_once( ABSPATH . CONTENT . '/class/class.settings.php');
endif;

$Settings = new Settings();

if( !class_exists('Header') ):
	require_once( ABSPATH . CONTENT . '/class/class.header.php');
endif;

$Header = new Header();

if( !class_exists('Footer') ):
	require_once( ABSPATH . CONTENT . '/class/class.footer.php');
endif;

$Footer = new Footer();


if( !class_exists('Widgets') ):
	require_once( ABSPATH . CONTENT . '/class/class.widgets.php');
endif;

$Widgets = new Widgets();
?>