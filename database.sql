-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 08, 2018 at 01:35 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `therapy`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_access_log`
--

CREATE TABLE `tbl_access_log` (
  `ID` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `ip_address` text COLLATE utf8_unicode_ci NOT NULL,
  `device` text COLLATE utf8_unicode_ci NOT NULL,
  `user_agent` text COLLATE utf8_unicode_ci NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_access_log`
--

INSERT INTO `tbl_access_log` (`ID`, `user_id`, `ip_address`, `device`, `user_agent`, `date`) VALUES
(1, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '2017-03-05 17:43:59'),
(2, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '2017-03-21 17:01:21'),
(3, 1, '127.0.0.1', 'Desktop/Laptop', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/603.1.30 (KHTML, like Gecko) Version/10.1 Safari/603.1.30', '2017-04-17 22:55:40'),
(4, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/602.3.12 (KHTML, like Gecko) Version/10.0.2 Safari/602.3.12', '2017-05-15 14:48:46'),
(5, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/602.3.12 (KHTML, like Gecko) Version/10.0.2 Safari/602.3.12', '2017-05-16 13:18:48'),
(6, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/602.3.12 (KHTML, like Gecko) Version/10.0.2 Safari/602.3.12', '2017-05-16 14:04:06'),
(7, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/602.3.12 (KHTML, like Gecko) Version/10.0.2 Safari/602.3.12', '2017-05-16 15:19:21'),
(8, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/602.3.12 (KHTML, like Gecko) Version/10.0.2 Safari/602.3.12', '2017-05-17 09:58:39'),
(9, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/602.3.12 (KHTML, like Gecko) Version/10.0.2 Safari/602.3.12', '2017-05-17 10:02:03'),
(10, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/602.3.12 (KHTML, like Gecko) Version/10.0.2 Safari/602.3.12', '2017-05-17 10:03:34'),
(11, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/602.3.12 (KHTML, like Gecko) Version/10.0.2 Safari/602.3.12', '2017-05-17 10:14:10'),
(12, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/602.3.12 (KHTML, like Gecko) Version/10.0.2 Safari/602.3.12', '2017-05-17 10:14:21'),
(13, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/602.3.12 (KHTML, like Gecko) Version/10.0.2 Safari/602.3.12', '2017-05-17 14:33:10'),
(14, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/602.3.12 (KHTML, like Gecko) Version/10.0.2 Safari/602.3.12', '2017-05-23 14:59:39'),
(15, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:56.0) Gecko/20100101 Firefox/56.0', '2017-11-10 12:57:33'),
(16, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:56.0) Gecko/20100101 Firefox/56.0', '2017-11-10 13:01:12'),
(17, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:56.0) Gecko/20100101 Firefox/56.0', '2017-11-10 14:22:19'),
(18, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:56.0) Gecko/20100101 Firefox/56.0', '2017-11-10 14:40:47'),
(19, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:56.0) Gecko/20100101 Firefox/56.0', '2017-11-10 15:16:00'),
(20, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:56.0) Gecko/20100101 Firefox/56.0', '2017-11-10 15:27:52'),
(21, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:56.0) Gecko/20100101 Firefox/56.0', '2017-11-10 15:56:10'),
(22, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:56.0) Gecko/20100101 Firefox/56.0', '2017-11-15 12:55:10'),
(23, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0', '2017-12-13 13:58:06'),
(24, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0', '2017-12-13 13:58:38'),
(25, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0', '2017-12-13 14:40:02'),
(26, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '2018-02-07 17:04:18'),
(27, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '2018-02-12 12:35:21'),
(28, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '2018-02-20 16:34:42'),
(29, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '2018-02-22 15:01:34'),
(30, 10000579697, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '2018-02-22 15:41:33'),
(31, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '2018-02-24 23:22:57'),
(32, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '2018-02-26 23:23:42'),
(33, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '2018-03-01 15:32:48'),
(34, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '2018-03-04 14:09:55'),
(35, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '2018-03-04 17:42:59'),
(36, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '2018-03-04 17:46:02'),
(37, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '2018-03-08 15:17:25'),
(38, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '2018-03-11 19:17:58'),
(39, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '2018-03-22 13:57:43'),
(40, 101, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '2018-03-23 19:13:55'),
(41, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '2018-03-23 19:14:17'),
(42, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '2018-04-11 10:18:58'),
(43, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '2018-04-16 11:40:15'),
(44, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '2018-05-03 20:45:17'),
(45, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '2018-05-03 21:07:03'),
(46, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '2018-05-05 13:20:52'),
(47, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '2018-05-07 11:10:26'),
(48, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '2018-05-08 00:24:53'),
(49, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '2018-05-08 03:33:06'),
(50, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '2018-05-08 07:01:02'),
(51, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '2018-05-08 07:34:01'),
(52, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '2018-05-08 07:39:25'),
(53, 10000384920, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '2018-05-08 08:18:40'),
(54, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '2018-05-08 08:19:17');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_notifications`
--

CREATE TABLE `tbl_notifications` (
  `ID` int(11) NOT NULL,
  `user_id` bigint(15) NOT NULL,
  `title` varchar(150) NOT NULL,
  `notification` varchar(150) NOT NULL,
  `read` int(11) NOT NULL,
  `hide` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_notifications`
--

INSERT INTO `tbl_notifications` (`ID`, `user_id`, `title`, `notification`, `read`, `hide`, `date`) VALUES
(178, 1, 'Users Capabilities updated', 'You have successfully updated users capabilities.', 1, 1, '2017-03-05 17:44:56'),
(179, 1, 'Users Capabilities updated', 'You have successfully updated users capabilities.', 1, 1, '2017-03-05 17:45:03'),
(182, 1, 'Password Changed', 'You have successfully changed your account password.', 1, 1, '2017-05-19 09:51:51'),
(183, 1, 'General Setting updated', 'You have successfully updated website general settings.', 1, 1, '2017-05-22 12:27:39'),
(184, 1, 'Password Changed', 'You have successfully changed your account password.', 1, 1, '2017-05-23 08:09:33'),
(185, 1, 'New Account Created', 'You have successfully created a new account (Mazen Sehgal).', 1, 1, '2017-11-10 13:02:36'),
(186, 1, 'Account Details updated', 'You have successfully updated (Mazen Sehgal) account details.', 1, 1, '2017-11-10 13:05:57'),
(187, 1, 'Users Capabilities updated', 'You have successfully updated users capabilities.', 1, 1, '2017-11-10 14:42:57'),
(188, 1, 'General Setting updated', 'You have successfully updated website general settings.', 1, 1, '2017-11-10 14:44:14'),
(189, 1, 'Users Capabilities updated', 'You have successfully updated users capabilities.', 1, 1, '2017-11-10 14:45:52'),
(190, 1, 'Users Capabilities updated', 'You have successfully updated users capabilities.', 1, 1, '2017-11-10 14:47:14'),
(191, 1, 'Account Details updated', 'You have successfully updated (Mazen Sehgal) account details.', 1, 1, '2017-11-10 14:55:26'),
(192, 1, 'Account Details updated', 'You have successfully updated (Mazen Sehgal) account details.', 1, 1, '2017-11-10 15:16:15'),
(193, 1, 'Users Capabilities updated', 'You have successfully updated users capabilities.', 1, 1, '2017-11-10 15:16:27'),
(194, 1, 'General Setting updated', 'You have successfully updated website general settings.', 1, 1, '2017-11-10 15:18:32'),
(195, 1, 'General Setting updated', 'You have successfully updated website general settings.', 1, 1, '2017-11-10 15:19:16'),
(196, 1, 'General Setting updated', 'You have successfully updated website general settings.', 1, 1, '2017-11-10 15:19:37'),
(197, 1, 'New network mount point Created', 'You have successfully created a new Mount point ( /test).', 1, 1, '2018-02-20 17:23:24'),
(198, 1, 'Network deleted', 'You have successfully deleted this network.', 1, 1, '2018-02-20 17:33:24'),
(199, 1, 'New network mount point Created', 'You have successfully created a new Mount point ( /test).', 1, 1, '2018-02-20 17:34:24'),
(200, 1, 'New network mount point Created', 'You have successfully created a new Mount point ( /new).', 1, 1, '2018-02-20 17:34:29'),
(201, 1, 'New network mount point Created', 'You have successfully created a new Mount point ( /indeed).', 1, 1, '2018-02-20 17:34:32'),
(202, 1, 'Network deleted', 'You have successfully deleted this network.', 1, 1, '2018-02-20 17:34:51'),
(203, 1, 'Network Details updated', 'You have successfully updated ( /new1234567) network details.', 1, 1, '2018-02-20 18:00:42'),
(204, 1, 'Network Details updated', 'You have successfully updated ( /net) network details.', 1, 1, '2018-02-20 18:00:53'),
(205, 1, 'Network Details updated', 'You have successfully updated ( /net2) network details.', 1, 1, '2018-02-20 18:01:10'),
(206, 1, 'New network mount point Created', 'You have successfully created a new Mount point ( /).', 1, 1, '2018-02-20 23:47:21'),
(207, 1, 'New network mount point Created', 'You have successfully created a new Mount point ( /).', 1, 1, '2018-02-20 23:49:40'),
(208, 1, 'New database point Created', 'You have successfully created a new Mount point ( /test).', 1, 1, '2018-02-20 23:51:35'),
(209, 1, 'New network mount point Created', 'You have successfully created a new Mount point ( /test).', 1, 1, '2018-02-20 23:58:46'),
(210, 1, 'New network mount point Created', 'You have successfully created a new Mount point ( /test).', 1, 1, '2018-02-21 00:02:53'),
(211, 1, 'New network mount point Created', 'You have successfully created a new Mount point ( /new1).', 1, 1, '2018-02-21 00:03:28'),
(212, 1, 'New network mount point Created', 'You have successfully created a new Mount point ( /test1).', 1, 1, '2018-02-21 00:04:10'),
(213, 1, 'Network Details updated', 'You have successfully updated ( /test2) network details.', 1, 1, '2018-02-21 00:09:29'),
(214, 1, 'Network deleted', 'You have successfully deleted this database.', 1, 1, '2018-02-21 00:26:01'),
(215, 1, 'New database point Created', 'You have successfully created a new database ( /nope).', 1, 1, '2018-02-21 00:26:17'),
(216, 1, 'Database Details updated', 'You have successfully updated ( /yes) Server details.', 1, 1, '2018-02-21 00:29:34'),
(217, 1, 'New Server Created', 'You have successfully created a new Server ( /test).', 1, 1, '2018-02-21 00:43:58'),
(218, 1, 'Database Details updated', 'You have successfully updated ( /test) Server details.', 1, 1, '2018-02-21 00:50:57'),
(219, 1, 'Database Details updated', 'You have successfully updated ( /bob) Server details.', 1, 1, '2018-02-21 01:18:39'),
(220, 1, 'Network deleted', 'You have successfully deleted this server.', 1, 1, '2018-02-21 01:18:53'),
(221, 1, 'Users Capabilities updated', 'You have successfully updated users capabilities.', 0, 0, '2018-02-22 15:35:07'),
(222, 1, 'New Account Created', 'You have successfully created a new account (Test Test).', 0, 0, '2018-02-22 15:40:53'),
(223, 1, 'User Password Reset', 'You have successfully changed (Test Test) account password.', 0, 0, '2018-02-22 15:41:16'),
(224, 1, 'Account Details updated', 'You have successfully updated (Test Test) account details.', 0, 0, '2018-02-22 15:41:16'),
(225, 1, 'Users Capabilities updated', 'You have successfully updated users capabilities.', 0, 0, '2018-02-22 15:42:11'),
(226, 1, 'Users Capabilities updated', 'You have successfully updated users capabilities.', 0, 0, '2018-02-22 19:44:13'),
(227, 1, 'New Server Created', 'You have successfully created a new Server ( /Test).', 0, 0, '2018-02-24 23:46:03'),
(228, 1, 'Network deleted', 'You have successfully deleted this server.', 0, 0, '2018-02-24 23:47:30'),
(229, 1, 'New Server Created', 'You have successfully created a new Server ( /trial).', 0, 0, '2018-02-24 23:47:45'),
(230, 1, 'New Server Created', 'You have successfully created a new Server ( /test).', 0, 0, '2018-02-25 00:04:29'),
(231, 1, 'New Server Created', 'You have successfully created a new Server ( /new1).', 0, 0, '2018-02-25 00:04:43'),
(232, 1, 'New Server Created', 'You have successfully created a new Server ( /test1).', 0, 0, '2018-02-25 00:07:07'),
(233, 1, 'New Server Created', 'You have successfully created a new Server ( /test2).', 0, 0, '2018-02-25 00:07:24'),
(234, 1, 'New Server Created', 'You have successfully created a new Server ( /test3).', 0, 0, '2018-02-25 00:07:38'),
(235, 1, 'Database Details updated', 'You have successfully updated ( /test1) Server details.', 0, 0, '2018-02-25 00:28:28'),
(236, 1, 'Database Details updated', 'You have successfully updated ( /test2) Server details.', 0, 0, '2018-02-25 00:28:46'),
(237, 1, 'New Server Created', 'You have successfully created a new Server ( /Hostinger Server).', 0, 0, '2018-03-22 16:25:09'),
(238, 1, 'Database Details updated', 'You have successfully updated ( /localhost) Server details.', 0, 0, '2018-03-22 16:26:05'),
(239, 1, 'General Setting updated', 'You have successfully updated website general settings.', 0, 0, '2018-05-04 18:53:25'),
(240, 1, 'Admin Task completed', 'You have successfully Completed a task', 0, 0, '2018-05-08 02:34:12'),
(241, 1, 'Admin Task not completed', 'You have successfully marked as incomplete a task.', 0, 0, '2018-05-08 02:34:22'),
(242, 1, ' Task completed', 'You have successfully Completed a task', 0, 0, '2018-05-08 02:34:30'),
(243, 1, 'Admin Task completed', 'You have successfully Completed a task', 0, 0, '2018-05-08 02:34:35'),
(244, 1, 'Admin Task not completed', 'You have successfully marked as incomplete a task.', 0, 0, '2018-05-08 02:34:56'),
(245, 1, ' Task not completed', 'You have successfully marked as incomplete a task.', 0, 0, '2018-05-08 02:34:57'),
(246, 1, 'Task deleted', 'You have successfully deleted task (2).', 0, 0, '2018-05-08 02:44:11'),
(247, 1, 'Admin Task completed', 'You have successfully Completed a task', 0, 0, '2018-05-08 02:54:42'),
(248, 1, 'Admin Task not completed', 'You have successfully marked as incomplete a task.', 0, 0, '2018-05-08 02:54:47'),
(249, 1, 'Admin Task completed', 'You have successfully Completed a task', 0, 0, '2018-05-08 02:59:21'),
(250, 1, 'Admin Task not completed', 'You have successfully marked as incomplete a task.', 0, 0, '2018-05-08 03:05:44'),
(251, 1, 'Admin Task completed', 'You have successfully Completed a task', 0, 0, '2018-05-08 03:09:46'),
(252, 1, 'Admin Task not completed', 'You have successfully marked as incomplete a task.', 0, 0, '2018-05-08 03:09:51'),
(253, 1, 'Admin Task completed', 'You have successfully Completed a task', 0, 0, '2018-05-08 03:14:53'),
(254, 1, 'New picture added', 'You have successfully added a new picture.', 0, 0, '2018-05-08 06:03:40'),
(255, 1, 'New picture added', 'You have successfully added a new picture.', 0, 0, '2018-05-08 06:10:00'),
(256, 1, 'New picture added', 'You have successfully added a new picture.', 0, 0, '2018-05-08 06:18:54'),
(257, 1, 'picture deleted', 'You have successfully deleted picture (10000678089).', 0, 0, '2018-05-08 06:31:46'),
(258, 1, 'New picture added', 'You have successfully added a new picture.', 0, 0, '2018-05-08 06:33:39'),
(259, 1, 'picture deleted', 'You have successfully deleted picture ().', 0, 0, '2018-05-08 06:37:25'),
(260, 1, 'picture deleted', 'You have successfully deleted picture ().', 0, 0, '2018-05-08 06:39:27'),
(261, 1, 'New picture added', 'You have successfully added a new picture.', 0, 0, '2018-05-08 06:39:49'),
(262, 1, 'New picture added', 'You have successfully added a new picture.', 0, 0, '2018-05-08 06:39:58'),
(263, 1, 'New picture added', 'You have successfully added a new picture.', 0, 0, '2018-05-08 06:40:27'),
(264, 1, 'New picture added', 'You have successfully added a new picture.', 0, 0, '2018-05-08 06:40:33'),
(265, 1, 'New picture added', 'You have successfully added a new picture.', 0, 0, '2018-05-08 06:40:40'),
(266, 1, 'New picture added', 'You have successfully added a new picture.', 0, 0, '2018-05-08 06:40:47'),
(267, 1, 'picture deleted', 'You have successfully deleted picture (..).', 0, 0, '2018-05-08 06:50:41'),
(268, 1, 'picture deleted', 'You have successfully deleted picture (..).', 0, 0, '2018-05-08 06:54:32'),
(269, 1, 'picture deleted', 'You have successfully deleted picture (../content/uploads/user_images/1/img-1525761625.jpg).', 0, 0, '2018-05-08 06:58:35'),
(270, 1, 'picture deleted', 'You have successfully deleted picture (..//content/uploads/user_images/1/img-1525761631.jpg).', 0, 0, '2018-05-08 07:06:18'),
(271, 1, 'New picture added', 'You have successfully added a new picture.', 0, 0, '2018-05-08 07:06:32'),
(272, 1, 'New picture added', 'You have successfully added a new picture.', 0, 0, '2018-05-08 07:06:39'),
(273, 1, 'New picture added', 'You have successfully added a new picture.', 0, 0, '2018-05-08 07:06:47'),
(274, 1, 'New picture added', 'You have successfully added a new picture.', 0, 0, '2018-05-08 07:06:54'),
(275, 1, 'New picture added', 'You have successfully added a new picture.', 0, 0, '2018-05-08 07:07:01'),
(276, 1, 'picture deleted', 'You have successfully deleted picture (..//content/uploads/user_images/1/img-1525763197.jpg).', 0, 0, '2018-05-08 07:07:52'),
(277, 1, 'picture deleted', 'You have successfully deleted picture (../content/uploads/user_images/1/img-1525763205.png).', 0, 0, '2018-05-08 07:09:29'),
(278, 1, 'picture deleted', 'You have successfully deleted picture (../content/uploads/user_images/1/img-1525763212.jpg).', 0, 0, '2018-05-08 07:09:59'),
(279, 1, 'picture deleted', 'You have successfully deleted picture (../content/uploads/user_images/1/img-1525763219.jpg).', 0, 0, '2018-05-08 07:12:42'),
(280, 0, 'New Account Created', 'You have successfully created a new account (Maz Sehg).', 0, 0, '2018-05-08 08:12:43'),
(281, 0, 'New Account Created', 'You have successfully created a new account (Mazen Sehgal).', 0, 0, '2018-05-08 08:14:18'),
(282, 0, 'New Account Created', 'You have successfully created a new account (Mazen Sehgal).', 0, 0, '2018-05-08 08:15:36'),
(283, 0, 'New Account Created', 'You have successfully created a new account (Mazen Sehgal).', 0, 0, '2018-05-08 08:18:32'),
(284, 1, 'New picture added', 'You have successfully added a new picture.', 0, 0, '2018-05-08 08:38:53'),
(285, 1, 'Task deleted', 'You have successfully deleted task (10000089340).', 0, 0, '2018-05-08 08:40:56'),
(286, 1, 'New picture added', 'You have successfully added a new picture.', 0, 0, '2018-05-08 08:41:06');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_options`
--

CREATE TABLE `tbl_options` (
  `ID` int(11) NOT NULL,
  `option_name` text NOT NULL,
  `option_value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_options`
--

INSERT INTO `tbl_options` (`ID`, `option_name`, `option_value`) VALUES
(13, 'users_capabilities', 's:1757:\"a:5:{s:5:\"admin\";a:14:{s:9:\"view_user\";i:0;s:8:\"add_user\";i:0;s:9:\"edit_user\";i:0;s:12:\"view_centres\";i:0;s:16:\"view_connections\";i:0;s:11:\"view_server\";i:0;s:10:\"add_server\";i:0;s:11:\"edit_server\";i:0;s:12:\"view_network\";i:0;s:11:\"add_network\";i:0;s:12:\"edit_network\";i:0;s:13:\"view_database\";i:0;s:12:\"add_database\";i:0;s:13:\"edit_database\";i:0;}s:16:\"hospital_manager\";a:14:{s:9:\"view_user\";i:0;s:8:\"add_user\";i:0;s:9:\"edit_user\";i:0;s:12:\"view_centres\";i:0;s:16:\"view_connections\";i:0;s:11:\"view_server\";i:1;s:10:\"add_server\";i:1;s:11:\"edit_server\";i:0;s:12:\"view_network\";i:0;s:11:\"add_network\";i:0;s:12:\"edit_network\";i:0;s:13:\"view_database\";i:0;s:12:\"add_database\";i:0;s:13:\"edit_database\";i:0;}s:12:\"data_manager\";a:14:{s:9:\"view_user\";i:1;s:8:\"add_user\";i:0;s:9:\"edit_user\";i:0;s:12:\"view_centres\";i:0;s:16:\"view_connections\";i:0;s:11:\"view_server\";i:0;s:10:\"add_server\";i:1;s:11:\"edit_server\";i:0;s:12:\"view_network\";i:0;s:11:\"add_network\";i:0;s:12:\"edit_network\";i:0;s:13:\"view_database\";i:0;s:12:\"add_database\";i:0;s:13:\"edit_database\";i:0;}s:12:\"general_user\";a:14:{s:9:\"view_user\";i:0;s:8:\"add_user\";i:0;s:9:\"edit_user\";i:0;s:12:\"view_centres\";i:0;s:16:\"view_connections\";i:0;s:11:\"view_server\";i:0;s:10:\"add_server\";i:0;s:11:\"edit_server\";i:0;s:12:\"view_network\";i:0;s:11:\"add_network\";i:0;s:12:\"edit_network\";i:0;s:13:\"view_database\";i:0;s:12:\"add_database\";i:0;s:13:\"edit_database\";i:0;}s:12:\"default_user\";a:14:{s:9:\"view_user\";i:1;s:8:\"add_user\";i:1;s:9:\"edit_user\";i:0;s:12:\"view_centres\";i:1;s:16:\"view_connections\";i:0;s:11:\"view_server\";i:0;s:10:\"add_server\";i:0;s:11:\"edit_server\";i:0;s:12:\"view_network\";i:0;s:11:\"add_network\";i:0;s:12:\"edit_network\";i:0;s:13:\"view_database\";i:0;s:12:\"add_database\";i:0;s:13:\"edit_database\";i:0;}}\";'),
(14, 'site_name', 'Hackathon'),
(15, 'site_description', 'User panel for Hackathon members'),
(16, 'admin_email', 'admin@admin.com'),
(17, 'site_contact_email', 'admin@admin.com'),
(18, 'site_contact_phone', '0123123123'),
(19, 'site_domain', 'www.hotmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pictures`
--

CREATE TABLE `tbl_pictures` (
  `ID` bigint(20) NOT NULL,
  `picture` varchar(200) DEFAULT NULL,
  `user_ID` bigint(20) DEFAULT NULL,
  `added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pictures`
--

INSERT INTO `tbl_pictures` (`ID`, `picture`, `user_ID`, `added`) VALUES
(10000869971, '/content/uploads/user_images/1/img-1525763190.jpg', 1, '2018-05-08 07:06:32');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_todo_list`
--

CREATE TABLE `tbl_todo_list` (
  `ID` bigint(20) NOT NULL,
  `task` varchar(100) NOT NULL,
  `user_ID` bigint(20) NOT NULL,
  `priority` int(1) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_todo_list`
--

INSERT INTO `tbl_todo_list` (`ID`, `task`, `user_ID`, `priority`, `status`, `added`) VALUES
(1, 'Wake up', 1, 3, 1, '2018-05-08 02:17:27'),
(2, 'Sleep', 1, 3, 0, '2018-05-08 02:17:27'),
(3, 'Watch netflix', 1, 1, 0, '2018-05-08 02:17:27'),
(4, 'Eat breakfast', 1, 2, 0, '2018-05-08 02:17:27'),
(10000620423, 'test', 1, 3, 0, '2018-05-08 08:41:05');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_usermeta`
--

CREATE TABLE `tbl_usermeta` (
  `ID` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `meta_key` text NOT NULL,
  `meta_value` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_usermeta`
--

INSERT INTO `tbl_usermeta` (`ID`, `user_id`, `meta_key`, `meta_value`) VALUES
(30, 1, 'gender', 'Male'),
(31, 1, 'dob', '2017-05-08 12:00:00'),
(32, 1, 'user_phone', '123456879'),
(33, 1, 'profile_img', '/content/assets/img/user.png'),
(34, 10000382952, 'gender', 'Female'),
(35, 10000382952, 'dob', '2017-05-01 12:00:00'),
(36, 10000382952, 'user_phone', '123456789'),
(37, 10000382952, 'profile_img', ''),
(53, 10000662909, 'profile_img', ''),
(52, 10000662909, 'user_phone', '12345667'),
(50, 10000662909, 'gender', 'Male'),
(51, 10000662909, 'dob', '2017-05-08 12:00:00'),
(60, 10000724652, 'user_phone', '123456789'),
(59, 10000724652, 'dob', '2017-05-01 12:00:00'),
(58, 10000724652, 'gender', 'Male'),
(61, 10000724652, 'profile_img', ''),
(62, 10000766054, 'gender', 'Male'),
(63, 10000766054, 'dob', '1996-01-12 12:00:00'),
(64, 10000766054, 'user_phone', '07510784241'),
(65, 10000766054, 'profile_img', '/content/assets/img/user.png'),
(66, 10000579697, 'gender', 'Male'),
(67, 10000579697, 'dob', '2018-02-01 12:00:00'),
(68, 10000579697, 'user_phone', '123456789'),
(69, 10000579697, 'profile_img', '/content/assets/img/user.png'),
(70, 10000949073, 'profile_img', ''),
(71, 10000705595, 'profile_img', ''),
(72, 10000802667, 'profile_img', ''),
(73, 10000384920, 'profile_img', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `ID` bigint(20) NOT NULL,
  `user_email` varchar(512) NOT NULL,
  `user_pass` varchar(512) NOT NULL,
  `first_name` varchar(256) NOT NULL,
  `last_name` varchar(256) NOT NULL,
  `user_role` varchar(256) DEFAULT 'hospital_manager',
  `user_status` int(11) NOT NULL DEFAULT '1',
  `created_by` bigint(20) NOT NULL,
  `username` varchar(150) DEFAULT NULL,
  `registered_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_salt` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`ID`, `user_email`, `user_pass`, `first_name`, `last_name`, `user_role`, `user_status`, `created_by`, `username`, `registered_at`, `user_salt`) VALUES
(1, 'mazen_sehgal@hotmail.com', 'fab2b43cce5966b03432607b71fde4b298ec2f79a4f5a76eb6eed0c339cb0ce6', 'Mazen', 'Sehgal', 'admin', 1, 1, 'admin', '2017-11-10 13:02:36', '78cHgqMhLRJHz575WXy9uw=='),
(10000384920, 'qwerty@qwerty.com', '81637b92d4d06cf7a0d8f0dc8a1b0f2378c8ac00c91482bc472c51e870bb6405', 'Mazen', 'Sehgal', 'hospital_manager', 1, 0, 'qwerty', '2018-05-08 08:18:32', 'aFINs2iNFD7AmXgs23xiRg==');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_access_log`
--
ALTER TABLE `tbl_access_log`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_notifications`
--
ALTER TABLE `tbl_notifications`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_options`
--
ALTER TABLE `tbl_options`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_pictures`
--
ALTER TABLE `tbl_pictures`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_todo_list`
--
ALTER TABLE `tbl_todo_list`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_usermeta`
--
ALTER TABLE `tbl_usermeta`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_access_log`
--
ALTER TABLE `tbl_access_log`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `tbl_notifications`
--
ALTER TABLE `tbl_notifications`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=287;

--
-- AUTO_INCREMENT for table `tbl_options`
--
ALTER TABLE `tbl_options`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `tbl_pictures`
--
ALTER TABLE `tbl_pictures`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2147483647;

--
-- AUTO_INCREMENT for table `tbl_todo_list`
--
ALTER TABLE `tbl_todo_list`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2147483647;

--
-- AUTO_INCREMENT for table `tbl_usermeta`
--
ALTER TABLE `tbl_usermeta`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
